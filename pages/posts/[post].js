import React from 'react'
import Head from 'next/head'
import fetch from 'node-fetch'

export async function getStaticProps(context) {
    const post = context.params.post;
    console.log('Preview: ', context.preview);
    if (context.preview) {
        const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post}`);
        const data = await response.json();
        console.log('!! preview works in version 9.3.4');
        return {
            props: {
                data
            }
        }
    }
    console.log('!! preview does not work in version > 9.3.4');
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post}`);
    const data = await response.json();
    console.log(data);
    return {
        props: {
            data
        }
    }
}

export async function getStaticPaths() {
    const response = await fetch('https://jsonplaceholder.typicode.com/posts');
    const posts = await response.json();
    const paths = posts.map((post) => `/posts/${post.id}`).filter((_, index) => index <= 10);
    console.log(paths);
    return {
        paths,
        fallback: true
    };
}

const Post = (props) => (
    <div>
        <Head>
            <title>User</title>
            <link rel='icon' href='/favicon.ico' />
        </Head>
        <div className='hero'>
            {JSON.stringify(props)}
        </div>
    </div>
)

export default Post
