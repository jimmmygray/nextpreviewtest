export default (req, res) => {
    res.setPreviewData({
        slug: req.query.slug
    })
    res.writeHead(307, { Location: `/posts/${req.query.slug}` })
    res.end('Preview mode enabled')
}
